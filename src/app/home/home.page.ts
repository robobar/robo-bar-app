import { Component } from '@angular/core';
import { Plugins, CameraResultType, CameraSource, CameraDirection } from '@capacitor/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from '../api.service';
import { ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  photo;
  drinkOrder = 'select';

  drinks = [
    {name: 'Bourbon & Coke', value: 'bourbon-and-coke'},
    {name: 'Bourbon & Ginger ale', value: 'bourbon-and-ginger-ale'},
    {name: 'Gin & Tonic', value: 'gin-and-tonic'},
    {name: 'Margarita', value: 'margarita'},
    {name: 'Mojito', value: 'mopjito'},
    {name: 'Moscow Mule', value: 'moscow-mule'},
    {name: 'Old Fashioned', value: 'old-fashioned'},
    {name: 'Vodka & Soda', value: 'vodka-soda'},
    {name: 'Vodka & Cranberry', value: 'vodka-cranberry'},
    {name: 'Michelob Ultra', value: 'michelob-ultra'},
    {name: 'Bud Light', value: 'bud-light'},
    {name: 'Sycamore Brewery - Countryside IPA', value: 'sycamore-countrysideIPA'},
    {name: 'Triple C - 3C IPA', value: 'tripleC-3C-IPA'},
    {name: 'Triple C - Golden Boy Blonde Ale', value: 'tripleC-golden-boy-blonde-ale'},
    {name: 'Rapa Nui - Merlot', value: 'Rapa-Nui-Merlot'},
    {name: 'Vento - Pinot Noir', value: 'Vento-Pinot-Noir'},
    {name: 'Rapa Nui - Chardonnay', value: 'Rapa-Nui-Chardonnay'},
    {name: 'Rock Nest - Rose', value: 'Rock-Nest-Rose'},
    {name: 'Cava - Sparkling Champagne', value: 'Cava-Sparkling-Champagne'},
  ]

  constructor(private api: ApiService, private toastController: ToastController, private loadingController: LoadingController) {}

  async takePicture(event) {
    if (event.target.value !== 'select') {
      const image = await Plugins.Camera.getPhoto({
        quality: 100,
        allowEditing: false,
        resultType: CameraResultType.DataUrl,
        source: CameraSource.Camera,
        width: 720,
        correctOrientation: true,
        direction: CameraDirection.Front
      }).catch(() => this.drinkOrder = 'select');

      const loading = await this.loadingController.create({
        message: 'Registering face to drink...',
        duration: 9000
      });
      await loading.present();

      this.api.sendOrder(image, this.drinkOrder).subscribe(async res => {
        console.log(res);
        loading.dismiss();
        this.drinkOrder = 'select';
        const toast = await this.toastController.create({
          message: 'Drink submitted! The refill station will now recognize you.',
          duration: 2000
        });
        toast.present();
      });
    }
  }

}
